# Postman Things

## FMG.Communication
Import these files to add a collection and environment to your Postman client. It includes a pre-request script to generate a signed JWT for UAT calls.

- `Comm.postman_collection.json`
- `Comm.postman_environment.json`